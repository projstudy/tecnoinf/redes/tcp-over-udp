/// the following are UBUNTU/LINUX, and MacOS ONLY terminal color codes.
#define RESET "\033[0m"
#define BLACK "\033[30m"              /* Black */
#define RED "\033[31m"                /* Red */
#define GREEN "\033[32m"              /* Green */
#define YELLOW "\033[33m"             /* Yellow */
#define BLUE "\033[34m"               /* Blue */
#define MAGENTA "\033[35m"            /* Magenta */
#define CYAN "\033[36m"               /* Cyan */
#define WHITE "\033[37m"              /* White */
#define BOLDBLACK "\033[1m\033[30m"   /* Bold Black */
#define BOLDRED "\033[1m\033[31m"     /* Bold Red */
#define BOLDGREEN "\033[1m\033[32m"   /* Bold Green */
#define BOLDYELLOW "\033[1m\033[33m"  /* Bold Yellow */
#define BOLDBLUE "\033[1m\033[34m"    /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m" /* Bold Magenta */
#define BOLDCYAN "\033[1m\033[36m"    /* Bold Cyan */
#define BOLDWHITE "\033[1m\033[37m"   /* Bold White */

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>
#include "packet.hh"
#define PORT 8080

void sendFile();
/**
 *
 *   CAPA IP
 *
 */
int sock_fd;
sockaddr_in serverAddr;
int len = sizeof(serverAddr);

std::vector<Packet*> clientBuffer;
std::vector<Packet*> serverBuffer;
std::vector<std::string> text;
std::condition_variable cvIn;
std::condition_variable cvOut;

void createServer() {
  memset(&serverAddr, 0, sizeof(serverAddr));

  sock_fd = socket(AF_INET, SOCK_DGRAM, 0);

  if (sock_fd == -1) {
    std::cout << "Bad stuff" << std::endl;
    throw invalid_argument("Error al crear el socket");
  }

  serverAddr.sin_family = AF_INET;
  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  serverAddr.sin_port = htons(PORT);  //
}

void sender(string buffer) {
  if (sendto(sock_fd, (char*)buffer.c_str(), buffer.size(), 0,
             (const struct sockaddr*)&serverAddr, len) < 0) {
    std::cout << "No se pudo enviar" << std::endl;
  }
}

char* reciver() {
  char* buffer = new char[500];
  recvfrom(sock_fd, buffer, 500, 0, (struct sockaddr*)&serverAddr,
           (socklen_t*)&len);
  return buffer;
}

/**
 *
 *   CAPA TCP
 *
 */
bool operator<(Packet p1, Packet p2) {
  return p1.getSeq() < p2.getSeq();
}

std::vector<Packet> incommingPackets;
std::vector<Packet> outgoinPackets;
std::mutex lockIncomming;
std::mutex lockOutgoing;
std::mutex lockSeqAck;

int serverSeq = 5000;
int clientSeq = -1;
int initialSeq = 0;

std::atomic<bool> connectedToServer(false);
std::atomic<bool> runReciver(true);
std::atomic<bool> runSender(true);

void buildAndSend(Packet& p) {
  sender(p.encode());
  /* cout << GREEN << "[SENT] - " << RESET << p << endl; */
}

int incrementServerSeq(int x) {
  std::unique_lock<std::mutex> lock(lockSeqAck);
  serverSeq += x;
  return serverSeq;
}

int incrementClientSeq(int x) {
  std::unique_lock<std::mutex> lock(lockSeqAck);
  clientSeq += x;
  return clientSeq;
}

void tcpListen() {
  bool run = true;
  while (run) {
    string data = reciver();
    if (data.size() > 0) {
      Packet p = Packet(data);
      // cvIn.wait(lock, [] { return incommingPackets.size() > 0; }); //<##
      // after the wait, we own the lock.
      if (run) {
        std::unique_lock<std::mutex> lock(lockIncomming);
        incommingPackets.push_back(p);
        /* cout << RED << "[RCV] - " << RESET << p << endl; */
        lock.unlock();
      }
    }
    run = runReciver.load();
  }
}

void addToSend(Packet p) {
  // Wait until there is some space
  std::unique_lock<std::mutex> lock(lockOutgoing);
  while (outgoinPackets.size() >= 10) {
    cvOut.wait(lock);  // dureme hasta que otro thread lo despierte
  }
  if (std::find_if(outgoinPackets.begin(), outgoinPackets.end(),
                   [&p](Packet val) {
                     if (p.getSeq() == val.getSeq())
                       return true;
                     return false;
                   }) != outgoinPackets.end()) {
  } else {
    outgoinPackets.push_back(p);
  }
}

// checks if is a triple ack
bool isTripleAck(int& lastAck, int currentAck, int& ackCount) {
  if (lastAck == currentAck) {
    ackCount++;
  } else {
    ackCount = 1;
  }
  lastAck = currentAck;
  if (ackCount >= 3) {
    ackCount = 0;
    return true;
  } else {
    return false;
  }
}
int lastAck = -1;
int ackCount = 1;

void packetHandler(Packet p) {
  // HandShake?
  if (p.getFin() == 1) {
    int clientCount = incrementClientSeq(0);
    int serverCount = incrementServerSeq(0);
    if (p.getAck() == serverCount && p.getSeq() == clientCount) {
      /* std::cout << "FIN!" << std::endl; */
      int ack = p.getSeq();
      int seq = serverCount + 1;
      /* int length = 0; */
      Packet sentPack = Packet("", seq, ack, 0, 0, 1);
      // Packet sentPack = Packet("", seq, ack, length);
      buildAndSend(sentPack);
      // remove packet from cola
      {
        std::unique_lock<std::mutex> lock2(lockOutgoing);
        auto it = outgoinPackets.begin();
        while (it != outgoinPackets.end()) {
          Packet packet = *it;
          if (p.getAck() == packet.getSeq() + 1) {
            outgoinPackets.erase(it);
            break;
          }
          it++;
        }
      }
      runSender = false;
      runReciver = false;
      cvOut.notify_one();
      // Kill socket
      shutdown(sock_fd, SHUT_RDWR);
      close(sock_fd);
    }
  }
  if (p.getSyn() == 1) {
    /* std::cout << "SYN!" << std::endl; */
    int serverCount = incrementServerSeq(0);
    if (p.getAck() == serverCount + 1) {
      // save seq
      int ack = p.getSeq() + 1;
      int seq = serverCount + 1;
      int syn = 0;
      int length = 0;
      Packet sentPack = Packet("", seq, ack, length, syn);
      buildAndSend(sentPack);
      /* incrementServerSeq(1); */
      incrementClientSeq(p.getSeq() + 2);
      connectedToServer = true;
      std::cout << "Conexion establecida, descargando..." << std::endl;
    }
  } else {
    // Normal
    if (isTripleAck(lastAck, p.getAck(), ackCount)) {
      bool found = false;
      int count = 0;
      std::unique_lock<std::mutex> lock(lockOutgoing);
      while (!found && count < (int)outgoinPackets.size()) {
        if (outgoinPackets[count].getSeq() == p.getAck()) {
          outgoinPackets[count].setSent(false);
          found = true;
        }
        count++;
      }
    } else {
      int serverCount = incrementServerSeq(0);
      int clientCount = incrementClientSeq(0);
      if (p.getSeq() == clientCount && p.getAck() == serverCount &&
          p.getSyn() == 0 && p.getLen() == 0) {
      } else {
        if (p.getSeq() == clientCount) {
          clientCount = incrementClientSeq(p.getLen());
          int ack = clientCount;
          int seq = serverCount;
          int length = 0;
          Packet sentPack = Packet("", seq, ack, length);
          buildAndSend(sentPack);
          text.push_back(p.getData());
        }
      }
    }
  }
}

void watchOutGoing() {
  bool run = true;
  while (run) {
    // entre 1 y 9 segundos
    int time = rand() % 9 + 1;
    std::chrono::seconds timespan(time);
    std::this_thread::sleep_for(timespan);
    // entre 1 y 3 packets
    int qtyPackets = rand() % 4 + 1;
    //
    std::unique_lock<std::mutex> lk(lockOutgoing);
    int size = (int)outgoinPackets.size();
    if (size < qtyPackets) {
      qtyPackets = size;
    }
    int i = 0;
    while (qtyPackets > 0 && i < size) {
      if (!outgoinPackets[i].wasSent()) {
        buildAndSend(outgoinPackets[i]);
        outgoinPackets[i].setSent(true);
        qtyPackets--;
      }
      if (qtyPackets == 0) {
        break;
      }
      i++;
    }
    lk.unlock();
    run = runSender.load();
  }
}

void watchIncomming() {
  bool run = true;
  while (run) {
    // entre 1 y 9 segundos
    int time = rand() % 6 + 1;
    std::chrono::seconds timespan(time);
    std::this_thread::sleep_for(timespan);
    // entre 1 y 3 packets
    int qtyPackets = rand() % 3 + 1;
    //
    std::unique_lock<std::mutex> lk(lockIncomming);
    int size = (int)incommingPackets.size();
    if (size < qtyPackets) {
      qtyPackets = size;
    }
    for (int i = 0; i < qtyPackets; i++) {
      // accedo al primer packet
      auto p = incommingPackets.begin();
      if (p->isValid()) {
        lk.unlock();
        // proceso el packet
        packetHandler(*p);
        lk.lock();
        // lo elimino
        incommingPackets.erase(p);
      } else {
        std::cout << "ta roto" << std::endl;
        incommingPackets.erase(p);
      }
    }
    lk.unlock();
    // free lock
    run = runReciver.load();
    // hay lugar libre papu
    cvIn.notify_one();
  }
}

void timeOut() {
  bool run = true;
  while (run) {
    // timeout fijo en 6 segundos
    std::this_thread::sleep_for(std::chrono::seconds(6));
    std::unique_lock<std::mutex> lock(lockOutgoing);
    if (outgoinPackets.size() > 0) {
      buildAndSend(outgoinPackets[0]);
    }
    run = runSender.load();
  }
}

void connectToServer() {
  if (connect(sock_fd, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) < 0) {
    printf("\n Error : Connect Failed \n");
    exit(0);
  }
  printf("Conectando...\n");

  while (!connectedToServer.load()) {
    {
      // connect to server
      std::unique_lock<std::mutex> lk(lockSeqAck);
      Packet p = Packet("", serverSeq, 0, 0, 1);
      buildAndSend(p);
    }
    std::this_thread::sleep_for(std::chrono::seconds(6));
  }
  std::unique_lock<std::mutex> lk(lockSeqAck);
  serverSeq++;
}

/*

  APLICACION

*/

int main() {
  createServer();
  std::thread t4(connectToServer);
  std::thread t(tcpListen);
  std::thread t1(watchIncomming);
  std::thread t2(watchOutGoing);
  std::thread t3(timeOut);
  t4.join();
  t.join();
  t1.join();
  t2.join();
  t3.join();
  for (const auto& line : text) {
    std::cout << line << std::endl;
  }
  return 0;
}
