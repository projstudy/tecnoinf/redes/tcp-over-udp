#include "packet.hh"
#include <algorithm>
#include <arpa/inet.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string.h>
#include <sys/socket.h>

using namespace std;

void Packet::setSent(bool sent) { this->sent = sent; }
void Packet::setSeq(int seq) { this->seq = seq; }
void Packet::setData(string data) { this->data = data; }
void Packet::setAck(int ack) { this->ack = ack; }
void Packet::setSyn(int syn) { this->syn = syn; }
void Packet::setLen(int len) { this->len = len; }
void Packet::setFin(int fin) { this->fin = fin; }
void Packet::setCrc(int crc) { this->crc = crc; }

bool Packet::wasSent() { return this->sent; }
int Packet::getLen() { return this->len; }
int Packet::getSyn() { return this->syn; }
int Packet::getAck() { return this->ack; }
int Packet::getSeq() { return this->seq; }
int Packet::getFin() { return this->fin; }
int Packet::getCrc() { return this->crc; }

string Packet::getData() { return this->data; }
Packet::~Packet() {}
Packet::Packet() {}

Packet::Packet(string packet) {
  std::string delimiter = "<=>";
  size_t pos = 0;
  std::string token;
  // seq
  pos = packet.find(delimiter);
  token = packet.substr(0, pos);
  this->setSeq(stoi(token));
  packet.erase(0, pos + delimiter.length());
  // ack
  pos = packet.find(delimiter);
  token = packet.substr(0, pos);
  this->setAck(stoi(token));
  packet.erase(0, pos + delimiter.length());
  // data
  pos = packet.find(delimiter);
  token = packet.substr(0, pos);
  this->setData(token);
  packet.erase(0, pos + delimiter.length());
  // crc
  pos = packet.find(delimiter);
  token = packet.substr(0, pos);
  this->setCrc(stoi(token));
  packet.erase(0, pos + delimiter.length());
  // len
  pos = packet.find(delimiter);
  token = packet.substr(0, pos);
  this->setLen(stoi(token));
  packet.erase(0, pos + delimiter.length());
  // syn
  pos = packet.find(delimiter);
  token = packet.substr(0, pos);
  this->setSyn(stoi(token));
  packet.erase(0, pos + delimiter.length());
  // fin
  pos = packet.find(delimiter);
  token = packet.substr(0, pos);
  this->setFin(stoi(token));
  packet.erase(0, pos + delimiter.length());
  this->sent = false;
}

Packet::Packet(string data, int seq, int ack, int len, int syn, int fin) {
  this->data = data;
  this->seq = seq;
  this->ack = ack;
  this->syn = syn;
  this->fin = fin;
  this->len = len;
  this->sent = false;
}

ostream &operator<<(ostream &stream, Packet &packet) {
  return stream << "Seq: " << packet.getSeq() << "; Ack: " << packet.getAck()
                << "; Fin: " << packet.getFin() << "; Sync: " << packet.getSyn()
                << "; Len: " << packet.getLen() << "; Crc: " << packet.getCrc()
                << "; Data: " << packet.getData() << endl;
};

uint16_t crc16_update(uint16_t crc, uint8_t a) {
  int i;
  crc ^= a;
  for (i = 0; i < 8; ++i) {
    if (crc & 1)
      crc = (crc >> 1) ^ 0xA001;
    else
      crc = (crc >> 1);
  }
  return crc;
}

string Packet::encode() {
  int seq = this->getSeq();
  int ack = this->getAck();
  int len = this->getLen();
  int syn = this->getSyn();
  int fin = this->getFin();

  string finS = to_string(fin);
  string synS = to_string(syn);
  string lenS = to_string(len);
  string seqS = to_string(seq);
  string ackS = to_string(ack);

  string data = this->getData();

  int crc = 0;
  for (int i = 0; i < int(data.size()); ++i) {
    crc += data[i];
  }

  this->setCrc(crc);
  string crcS = to_string(crc);

  // 25% de chance de romper packet
  if (data.size() > 0 && rand() % 100 < 25) {
    int toRemove = int(rand() % data.size());
    data = data.erase(0, toRemove);
  }

  return seqS + "<=>" + ackS + "<=>" + data + "<=>" + crcS + "<=>" + lenS +
         "<=>" + synS + "<=>" + finS;
}

bool Packet::isValid() {
  string data = this->getData();
  int crc= 0;
  for (int i = 0; i < int(data.size()); ++i) {
    crc += data[i];
  }
  return crc == this->getCrc();
}
