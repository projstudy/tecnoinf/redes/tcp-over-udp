CC = g++
CFLAGS = -Wall -MMD -v

error:
	@echo "Please choose one of the following target: client, server"
	@# Alternatively: "Please RTFI(nstall)F(ile)"
	@exit 

client: client.cpp packet.o 
	$(CC) client.cpp -o $@ packet.o -pthread

server: server.cpp packet.o
	$(CC) server.cpp -o $@ packet.o -pthread

packet.o: packet.cpp packet.hh
	$(CC) -c $< -o $@

clean: 
	rm -fr *.o client server
