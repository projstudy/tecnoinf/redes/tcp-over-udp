#include <arpa/inet.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <sys/socket.h>

using namespace std;

class Packet {
  friend ostream &operator<<(ostream &stream, Packet &packet);
  friend bool operator<(Packet &p1, Packet &p2);

private:
  int seq;
  int ack;
  int syn;
  int len;
  int fin;
  int crc;
  bool sent;
  string data;

public:
  Packet();
  Packet(string data, int seq, int ack, int len = 0, int syn = 0, int fin = 0);
  Packet(string packet);

  void setSent(bool sent);
  bool wasSent();

  void setFin(int fin);
  int getFin();

  void setSeq(int seq);
  int getSeq();

  void setData(string data);
  string getData();

  void setAck(int ack);
  int getAck();

  void setSyn(int syn);
  int getSyn();

  void setLen(int len);
  int getLen();

  void setCrc(int crc);
  int getCrc();

  ~Packet();

  bool isValid();
  string encode();
};
