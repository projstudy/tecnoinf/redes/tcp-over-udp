/// the following are UBUNTU/LINUX, and MacOS ONLY terminal color codes.
#define RESET "\033[0m"
#define RED "\033[31m"   /* Red */
#define GREEN "\033[32m" /* Green */

#include "packet.hh"
#include <algorithm>
#include <arpa/inet.h>
#include <atomic>
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <thread>
#include <unistd.h>
#include <vector>
#define PORT 8080

void sendFile();
/**
 *
 *   CAPA IP
 *
 */
int sock_fd;
sockaddr_in serverAddr, clientAddr;
int len = sizeof(clientAddr);

void createServer() {
  memset(&serverAddr, 0, sizeof(serverAddr));
  memset(&clientAddr, 0, sizeof(clientAddr));

  sock_fd = socket(AF_INET, SOCK_DGRAM, 0);

  if (sock_fd == -1) {
    throw invalid_argument("Error al crear el socket");
  }

  serverAddr.sin_family = AF_INET;
  serverAddr.sin_addr.s_addr = INADDR_ANY;
  serverAddr.sin_port = htons(PORT); //

  if (bind(sock_fd, (sockaddr *)&serverAddr, sizeof(serverAddr)) == -1) {
    cerr << "Error al bindear" << endl;
  } else {
    cout << "server up" << endl;
  }
}

void sender(string buffer) {
  sendto(sock_fd, (char *)buffer.c_str(), buffer.size(), 0,
         (const struct sockaddr *)&clientAddr, sizeof(clientAddr));
}

char *reciver() {
  char *buffer = new char[500];
  recvfrom(sock_fd, buffer, 500, 0, (struct sockaddr *)&clientAddr,
           (socklen_t *)&len);
  return buffer;
}

/**
 *
 *   CAPA TCP
 *
 */
bool operator<(Packet p1, Packet p2) { return p1.getSeq() < p2.getSeq(); }

std::mutex lockEntrantes;
std::condition_variable notificarEntrantes;
std::vector<Packet> paquetesEntrantes;

std::mutex lockSalientes;
std::condition_variable notificarSalientes;
std::vector<Packet> paqutesSalientes;

std::mutex lockSeqAck;

// inicia en 0
int sequenciaServidor = 0;
// no la conozco
int sequenciaCliente = -1;

bool clienteConectado = false;
std::atomic<bool> escuchar(true);
std::atomic<bool> enviar(true);

void buildAndSend(Packet &p) {
  sender(p.encode());
  cout << GREEN << "[SENT] - " << RESET << p << endl;
}

/**
 * escucha paquetes y los encola para ser procesados
 * */
void tcpListen() {
  while (escuchar.load()) {
    string data = reciver();
    if (!data.empty()) {
      Packet p = Packet(data);
      std::unique_lock<std::mutex> lock(lockEntrantes);
      while (paquetesEntrantes.size() >= 10) {
        // dureme hasta que otro thread lo despierte
        notificarEntrantes.wait(lock);
      }
      paquetesEntrantes.push_back(p);
      cout << RED << "[RCV] - " << RESET << p << endl;
    }
  }
}

/**
 * encola paquete para su envio
 * */
void addToSend(Packet p) {
  // Wait until there is some space
  std::unique_lock<std::mutex> lock(lockSalientes);
  while (paqutesSalientes.size() >= 10) {
    notificarSalientes.wait(lock); // dureme hasta que otro thread lo despierte
  }
  // busco un paquete
  auto it =
      std::find_if(paqutesSalientes.begin(), paqutesSalientes.end(),
                   [&p](Packet val) { return p.getSeq() == val.getSeq(); });
  // si no encontre, lo agrego
  if (it == paqutesSalientes.end()) {
    paqutesSalientes.push_back(p);
  }
}

/**
 * Comprueba triple ack
 * */
bool isTripleAck(int &lastAck, int currentAck, int &ackCount) {
  if (lastAck == currentAck) {
    ackCount++;
  } else {
    ackCount = 1;
  }
  lastAck = currentAck;
  if (ackCount >= 3) {
    ackCount = 0;
    return true;
  } else {
    return false;
  }
}
int lastAck = -1;
int ackCount = 1;

/**
 * Funcion que toma acciones dependiendo del paquete que se esta precsado:
 * - Envia un handshake
 * - Envia Fin
 * - Envia datos
 * */
void packetHandler(Packet p) {
  // Quiere terminar la conexion?
  if (p.getFin() == 1) {
    int temServerSeq;
    {
      std::unique_lock<std::mutex> lock(lockSeqAck);
      temServerSeq = sequenciaServidor;
    }

    if (p.getAck() + 1 == temServerSeq) {
      int ack = p.getSeq();
      int seq = temServerSeq;
      int length = 0;
      Packet sentPack = Packet("", seq, ack, length);
      buildAndSend(sentPack);
      {
        std::unique_lock<std::mutex> lock2(lockSalientes);
        auto it = paqutesSalientes.begin();
        while (it != paqutesSalientes.end()) {
          Packet packet = *it;
          if (p.getAck() == packet.getSeq() + 1) {
            paqutesSalientes.erase(it);
            break;
          }
          it++;
        }
      }
      enviar = false;
      escuchar = false;
      notificarSalientes.notify_one();
      // Kill socket
      shutdown(sock_fd, SHUT_RDWR);
      close(sock_fd);
    }
  }
  //se quiere conectar?
  if (p.getSyn() == 1 && p.getAck() == 0) {
    // save seq
    int ack = p.getSeq() + 1;
    int seq = 0;
    int syn = 1;
    int length = 0;
    Packet sentPack = Packet("", seq, ack, length, syn);
    buildAndSend(sentPack);
    // update seq
    {
      std::unique_lock<std::mutex> lock(lockSeqAck);
      sequenciaCliente = ack;
      sequenciaServidor = 1;
    }
  }
  // confirma la conexion?
  else if (p.getSyn() == 0 && p.getAck() == 1 &&
           p.getSeq() == sequenciaCliente) {
    if (!clienteConectado) {
      int length = 0;
      int ack = p.getSeq();
      int seq;
      {
        std::unique_lock<std::mutex> lock(lockSeqAck);
        sequenciaCliente = ack;
        seq = sequenciaServidor;
      }
      Packet sentPack = Packet("", seq, ack, length);
      buildAndSend(sentPack);
      clienteConectado = true;
      // ahora puedo mandar el archivo
      std::thread t(sendFile);
      t.detach();
    }
  } else {
    // es un paquete normal?
    if (isTripleAck(lastAck, p.getAck(), ackCount)) {
      bool found = false;
      int count = 0;
      std::unique_lock<std::mutex> lock(lockSalientes);
      while (!found && count < (int)paqutesSalientes.size()) {
        if (paqutesSalientes[count].getSeq() == p.getAck()) {
          paqutesSalientes[count].setSent(false);
          found = true;
        }
        count++;
      }
    } else {
      // es una confirmacion de otro paquete?
      std::unique_lock<std::mutex> lk(lockSalientes);
      auto it = paqutesSalientes.begin();
      bool freeSpace = false;
      while (it != paqutesSalientes.end()) {
        Packet packet = *it;
        if (p.getAck() == packet.getLen() + packet.getSeq()) {
          paqutesSalientes.erase(it);
          freeSpace = true;
          break;
        }
        it++;
      }
      lk.unlock();
      if (freeSpace) {
        notificarSalientes.notify_one();
      }
    }
  }
}

void fileSender(string data) {
  Packet p;
  {
    std::unique_lock<std::mutex> lock(lockSeqAck);
    p = Packet(data, sequenciaServidor, sequenciaCliente, data.size());
    sequenciaServidor += data.size();
  }
  addToSend(p);
};

void watchOutGoing() {
  bool run = true;
  while (run) {
    // entre 1 y 9 segundos
    int time = rand() % 9 + 1;
    std::chrono::seconds timespan(time);
    std::this_thread::sleep_for(timespan);
    // entre 1 y 3 packets
    int qtyPackets = rand() % 4 + 1;
    //
    std::unique_lock<std::mutex> lk(lockSalientes);
    int size = (int)paqutesSalientes.size();
    if (size < qtyPackets) {
      qtyPackets = size;
    }
    // auto it = paqutesSalientes.begin();
    int i = 0;
    while (qtyPackets > 0 && i < size) {
      if (!paqutesSalientes[i].wasSent()) {
        buildAndSend(paqutesSalientes[i]);
        paqutesSalientes[i].setSent(true);
        qtyPackets--;
      }
      if (qtyPackets == 0) {
        break;
      }
      i++;
    }
    run = enviar.load();
  }
}

void watchIncomming() {
  bool run = true;
  while (run) {
    // entre 1 y 9 segundos
    int time = rand() % 6 + 1;
    std::chrono::seconds timespan(time);
    std::this_thread::sleep_for(timespan);
    // entre 1 y 3 packets
    int qtyPackets = rand() % 3 + 1;
    //
    std::unique_lock<std::mutex> lock(lockEntrantes);
    int size = (int)paquetesEntrantes.size();

    if (size < qtyPackets) {
      qtyPackets = size;
    }
    for (int i = 0; i < qtyPackets; i++) {
      // accedo al primer packet
      auto p = paquetesEntrantes.begin();
      // proceso el packet
      packetHandler(*p);
      // lo elimino
      paquetesEntrantes.erase(p);
    }
    lock.unlock();
    // free lock
    run = escuchar.load();
    // hay lugar libre papu
    notificarEntrantes.notify_one();
  }
}

void timeOut() {
  bool run = true;
  while (run) {
    // timeout fijo en 6 segundos
    std::this_thread::sleep_for(std::chrono::seconds(6));
    std::unique_lock<std::mutex> lock(lockSalientes);
    if (paqutesSalientes.size() > 0) {
      buildAndSend(paqutesSalientes[0]);
    }
    run = enviar.load();
  }
}

void tcpFin() {
  Packet p;
  {
    std::unique_lock<std::mutex> lk(lockSeqAck);
    p = Packet("", sequenciaServidor, sequenciaCliente, 0, 0, 1);
    sequenciaServidor += 1;
  }
  addToSend(p);
}

/*
  APLICACION
*/
void sendFile() {
  std::string line;
  std::ifstream infile("/home/forbi/tcp-over-udp/thefile.txt");
  while (std::getline(infile, line)) {
    fileSender(line);
  }
  tcpFin();
}

int main() {
  createServer();
  std::thread t(tcpListen);
  std::thread t1(watchIncomming);
  std::thread t2(watchOutGoing);
  std::thread t3(timeOut);
  t.join();
  t1.join();
  t2.join();
  t3.join();
  return 0;
}
